package com.ixl.ixlanimals;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gbrown on 10/13/16.
 */
public class ImageManager {

  private static final String TAG = ImageManager.class.getSimpleName();

  /*Sonika's Comment: I think, There is no need to make it static array as
  I am going to use it as private*/
  private final String[] IMAGE_URLS = new String[] {
      "bar/foo/baz/animal-001-dYlAANp.png",
      "foo/bar/baz/animal-002-unQLJIb.png",
      "pics/foo/baz/animal-003-aDwklHj.png",
      "bar/pics/animal-004-J5qweNZ.png",
      "pics/baz/foo/animal-006-fu6dIpB.png",
      "baz/bar/animal-007-xjFRrjz.png",
      "baz/bar/pics/animal-008-CPqbVW8.png",
      "baz/pics/bar/animal-011-Ckf5OeO.png",
      "pics/animal-012-3jq1bv7.png",
      "pics/animal-016-ntFfrav.png",
      "bar/animal-017-JfKH8wd.png",
      "bar/pics/foo/animal-018-KDfJruL.png"
  };

  // TODO #1
  // We need to filter out the image names from the list of
  // image urls according to the below rules:
  //    Image names follow the final "/" in the url.
  //    Image names use "_" instead of "-".
  //    Image names exclude the file type, ".png".
  //    Image names are all lowercase.

  /*Sonika's Comment: No need for this method as a static method*/
  public List<ImageModel> getImageNames() {
    String imageName;
    List<ImageModel> imageNamesList = new ArrayList<>();
    for (String IMAGE_URL : IMAGE_URLS) {
      imageName = IMAGE_URL;
      imageName = imageName.substring(imageName.lastIndexOf("/") + 1, imageName.indexOf("."));//This line is for getting the image names from urls and excluding the ".png"
      imageName = imageName.replaceAll("[^a-zA-Z0-9.]", "_");//This line is replacing the "-" with "_"
      imageName = imageName.toLowerCase(); //This line is for converting the image names in lowercase.

      imageNamesList.add(getLabelDescription(imageName));
    }
    return imageNamesList;
  }

  // TODO #2
  // Please rewrite getLabelDescription and getImageName to be "better". Submit your
  // replacement code, and please also submit a few brief comments
  // explaining why you think your code is better than the sample.

  /* Sonika's Comment: I do not need this method as static method. I need only a private
  reference of this method so making it a private method and I am using a
  model class(ImageDetailsModel.java) here to save the image names and the
  corresponding label description so that we can get image names and label
  description with the reference of the model class */

  private ImageModel getLabelDescription(String imageName) {
    Log.d("ImageManager", "Inside getLabelDescription = "+imageName);
    ImageModel imageDetailsModel = new ImageModel();
    switch (imageName) {
      case "animal_001_dylaanp":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Bunnie swarm");
        return imageDetailsModel;
      case "animal_002_unqljib":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Loud kittens");
        return imageDetailsModel;
      case "animal_003_adwklhj":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Walking duckling");
        return imageDetailsModel;
      case "animal_004_j5qwenz":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("White rabbit");
        return imageDetailsModel;
      case "animal_006_fu6dipb":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Kitty in bed");
        return imageDetailsModel;
      case "animal_007_xjfrrjz":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Raccoon_dog");
        return imageDetailsModel;
      case "animal_008_cpqbvw8":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Orange kitten");
        return imageDetailsModel;
      case "animal_011_ckf5oeo":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Ninja kitten");
        return imageDetailsModel;
      case "animal_012_3jq1bv7":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Dog and kitten");
        return imageDetailsModel;
      case "animal_016_ntffrav":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Orange cat");
        return imageDetailsModel;
      case "animal_017_jfkh8wd":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Little ears");
        return imageDetailsModel;
      case "animal_018_kdfjrul":
        imageDetailsModel.setImageName(imageName);
        imageDetailsModel.setImageDescription("Sleeping kitten");
        return imageDetailsModel;
    }
    return imageDetailsModel;
  }

  /*Sonika's Comment: I do not need this method*/

  /*static String getImageName(String labelDescription) {
    if (labelDescription.equals("Bunnie swarm")) { return "animal_001_dylaanp"; }
    if (labelDescription.equals("Loud kittens")) { return "animal_002_unqljib"; }
    if (labelDescription.equals("Walking duckling")) { return "animal_003_adwklhj"; }
    if (labelDescription.equals("White rabbit")) { return "animal_004_j5qwenz"; }
    if (labelDescription.equals("Kitty in bed")) { return "animal_006_fu6dipb"; }
    if (labelDescription.equals("Raccoon_dog")) { return "animal_007_xjfrrjz"; }
    if (labelDescription.equals("Orange kitten")) { return "animal_008_cpqbvw8"; }
    if (labelDescription.equals("Ninja kitten")) { return "animal_011_ckf5oeo"; }
    if (labelDescription.equals("Dog and kitten")) { return "animal_012_3jq1bv7"; }
    if (labelDescription.equals("Orange cat")) { return "animal_016_ntffrav"; }
    if (labelDescription.equals("Little ears")) { return "animal_017_jfkh8wd"; }
    if (labelDescription.equals("Sleeping kitten")) { return "animal_018_kdfjrul"; }
    return "";
  }*/


  // This method is provided for your convenience.
  // No need to modify.

  /*Sonika's Comment: I do not need this static method, I need it inside the
  AnimalListAdapter.java as a private method */

  /*static Drawable getImageDrawable(Context context, String imageName) {
    int resourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
    return context.getDrawable(resourceId);
  }*/
}
