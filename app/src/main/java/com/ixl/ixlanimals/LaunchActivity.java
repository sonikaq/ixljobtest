package com.ixl.ixlanimals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

/**
 * Created by gbrown on 10/17/16.
 */
public class LaunchActivity extends Activity {

  private ProgressBar progressBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // TODO #3
    // Give some visual indication that the app is loading the images.

    /*Sonika's Comment: For now, I am using a progressBar to give user
    a feel that data is loading*/
    setContentView(R.layout.activity_launch);

    progressBar = (ProgressBar)findViewById(R.id.progressBar);
    progressBar.setVisibility(View.VISIBLE);

    loadAnimalPictures();
  }

  // We pretend it takes 3 seconds to load the animal pictures.
  private void loadAnimalPictures() {
    Runnable showAnimalPictures = new Runnable() {
      @Override
      public void run() {
        startActivity(new Intent(LaunchActivity.this, AnimalPicturesActivity.class));
      }
    };
    new Handler().postDelayed(showAnimalPictures, 3000);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    progressBar.setVisibility(View.GONE);
  }
}
