package com.ixl.ixlanimals;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

public class AnimalPicturesActivity extends Activity implements AnimalDescriptionPresenter {

  private static final String TAG = AnimalPicturesActivity.class.getSimpleName();
  private List<ImageModel> imageNamesList;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_animal_pictures);
    setupRecyclerView();
  }

  // TODO #4
  // We need to show the user all of the images and labels
  // but there isn't enough room for them to fit on the screen
  // at the same time. Our current implementation doesn't work.
  //
  // Replace the current layout with a RecyclerView
  // to display 2 columns of images and labels at a time and
  // allow the user to vertically scroll through them.

  private void setupRecyclerView() {
    // The below code is for demonstration purposes and doesn't scale.
    // It should be deleted and replaced with your RecyclerView implementation.
   /* TextView animalItem0 = (TextView) (findViewById(R.id.item_0).findViewById(R.id.label));
    final String imageName0 = ImageManager.getImageNames().get(0);
    animalItem0.setText(ImageManager.getLabelDescription(imageName0));
    Drawable animalDrawable0 = ImageManager.getImageDrawable(this, imageName0);
    animalItem0.setCompoundDrawablesWithIntrinsicBounds(null, animalDrawable0 , null, null);
    animalItem0.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showImageName(ImageManager.getLabelDescription(imageName0));
      }
    });

    TextView animalItem1 = (TextView) (findViewById(R.id.item_1).findViewById(R.id.label));
    final String imageName1 = ImageManager.getImageNames().get(1);
    animalItem1.setText(ImageManager.getLabelDescription(imageName1));
    Drawable animalDrawable1 = ImageManager.getImageDrawable(this, imageName1);
    animalItem1.setCompoundDrawablesWithIntrinsicBounds(null, animalDrawable1 , null, null);
    animalItem1.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showImageName(ImageManager.getLabelDescription(imageName1));
      }
    });

    TextView animalItem2 = (TextView) (findViewById(R.id.item_2).findViewById(R.id.label));
    final String imageName2 = ImageManager.getImageNames().get(2);
    animalItem2.setText(ImageManager.getLabelDescription(imageName2));
    Drawable animalDrawable2 = ImageManager.getImageDrawable(this, imageName2);
    animalItem2.setCompoundDrawablesWithIntrinsicBounds(null, animalDrawable2 , null, null);
    animalItem2.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showImageName(ImageManager.getLabelDescription(imageName2));
      }
    });

    TextView animalItem3 = (TextView) (findViewById(R.id.item_3).findViewById(R.id.label));
    final String imageName3 = ImageManager.getImageNames().get(3);
    animalItem3.setText(ImageManager.getLabelDescription(imageName3));
    Drawable animalDrawable3 = ImageManager.getImageDrawable(this, imageName3);
    animalItem3.setCompoundDrawablesWithIntrinsicBounds(null, animalDrawable3 , null, null);
    animalItem3.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        showImageName(ImageManager.getLabelDescription(imageName3));
      }
    });*/

    ImageManager imageManager = new ImageManager();
    imageNamesList = imageManager.getImageNames();

    RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

    int numberOfColumns = 2;
    recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));

    AnimalListAdapter adapter = new AnimalListAdapter(this, imageNamesList);
    recyclerView.setAdapter(adapter);

  }

  // TODO #5
  // Include functionality that whenever a user "clicks" an
  // image or label there is a call to this method
  // with the labelDescription parameter corresponding to the click.
  // This method shouldn't need to be modified.
  // You can use this to verify the bidirectional mapping of image names to image labels.
  public void showImageName(String labelDescription) {
    Toast.makeText(this, labelDescription, Toast.LENGTH_SHORT).show();
  }

  // TODO #6 (optional)
  // Include any comments or suggestions for improvement to the job test here.

}
