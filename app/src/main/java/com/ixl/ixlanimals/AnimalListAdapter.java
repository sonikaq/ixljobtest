package com.ixl.ixlanimals;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

public class AnimalListAdapter extends RecyclerView.Adapter<AnimalListAdapter.ViewHolder> implements View.OnClickListener{

    private Context context;
    private List<ImageModel> imageDetailsModelList;

    public AnimalListAdapter(Context context, List<ImageModel> imageDetailsModelList) {
        this.context = context;
        this.imageDetailsModelList = imageDetailsModelList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView animalPictureImageView;
        TextView descriptionTextView;

        public ViewHolder(View itemView) {

            super(itemView);

            animalPictureImageView = (ImageView)itemView.findViewById(R.id.listItemImageView);
            descriptionTextView = (TextView)itemView.findViewById(R.id.listItemDespTextView);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.animal_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AnimalListAdapter.ViewHolder holder, final int position) {

        Drawable drawable = getImageDrawable(context, imageDetailsModelList.get(position).getImageName());

        holder.descriptionTextView.setText(imageDetailsModelList.get(position).getImageDescription());
        holder.animalPictureImageView.setImageDrawable(drawable);

        holder.animalPictureImageView.setOnClickListener(this);
        holder.animalPictureImageView.setTag(holder);

        holder.descriptionTextView.setOnClickListener(this);
        holder.descriptionTextView.setTag(holder);
    }

    @Override
    public int getItemCount() {

        return imageDetailsModelList.size();
    }

    @Override
    public void onClick(View view) {
        ViewHolder holder = (ViewHolder)view.getTag();
        int position = holder.getAdapterPosition();
        switch (view.getId()){
            case R.id.listItemImageView:
                ((AnimalPicturesActivity)context).showImageName("Image Name: "+imageDetailsModelList.get(position).getImageName());
                break;
            case R.id.listItemDespTextView:
                ((AnimalPicturesActivity)context).showImageName("Label Description: "+imageDetailsModelList.get(position).getImageDescription());
                break;

        }
    }

    private Drawable getImageDrawable(Context context, String imageName) {
        int resourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
        return context.getDrawable(resourceId);
    }
}
