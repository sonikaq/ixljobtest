package com.ixl.ixlanimals;

/**
 * Created by gbrown on 10/14/16.
 */
public interface AnimalDescriptionPresenter {

  void showImageName(String description);
}
